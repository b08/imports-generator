import { test } from "@b08/test-runner";
import { GeneratorOptions, ImportModel } from "../src";
import { tryImportFromIndex } from "../src/tryImportFromIndex";

const baseOptions: GeneratorOptions = {
  linefeed: "\n",
  quotes: "\""
};

test("tryImportFromIndex clips one segment off the import path, for one regex", expect => {
  // arrange
  const input: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" },
    { type: "Type1State", alias: "Type1State", importPath: "./type1.state" }
  ];
  const options = { ...baseOptions, importTypesFromIndex: /.+State/ };
  const expected: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" },
    { type: "Type1State", alias: "Type1State", importPath: "." }
  ];

  // act
  const result = tryImportFromIndex(input, options);

  // assert
  expect.deepEqual(result, expected);
});


test("tryImportFromIndex clips one segment off the import paths, for several regexes", expect => {
  // arrange
  const input: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" },
    { type: "Type1State", alias: "Type1State", importPath: "./some/path/type1.state" },
    { type: "reducer", alias: "reducer", importPath: "../../reducer" }
  ];
  const options = { ...baseOptions, importTypesFromIndex: [/.+State$/, /^reducer$/] };
  const expected: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" },
    { type: "Type1State", alias: "Type1State", importPath: "./some/path" },
    { type: "reducer", alias: "reducer", importPath: "../.." }
  ];

  // act
  const result = tryImportFromIndex(input, options);

  // assert
  expect.deepEqual(result, expected);
});

