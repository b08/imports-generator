import { describe } from "@b08/test-runner";
import { ImportModel } from "../../src";
import { SquashedImport } from "../../src/squash/squashedImport.type";
import { squashImports } from "../../src/squash/squashImports";
import { checkCompactImports } from "./checkCompactImports";

describe("squashImports", it => {
  it("should group and join single imports", async expect => {
    // arrange
    const input: ImportModel[] = [
      { type: "Type1", alias: "Type2", importPath: "./dir1" },
      { type: "Type3", alias: "Type4", importPath: "./dir2" },
      { type: "Type5", alias: "Type6", importPath: "./dir1" },
      { type: "Type7", alias: "Type8", importPath: "./dir2" }
    ];
    const expected: SquashedImport[] = [
      { isMultiline: false, types: ["Type1 as Type2, Type5 as Type6"], importPath: "./dir1" },
      { isMultiline: false, types: ["Type3 as Type4, Type7 as Type8"], importPath: "./dir2" }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });

  it("should not split single imports when length less than 120", async expect => {
    // arrange
    const input: ImportModel[] = [
      {
        type: "Type10123456789",
        alias: "Type10123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type20123456789",
        alias: "Type20123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type30123456789",
        alias: "Type30123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type40123456789",
        alias: "Type40123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
    ];
    const expected: SquashedImport[] = [
      {
        isMultiline: false,
        types: ["Type10123456789, Type20123456789, Type30123456789, Type40123456789"],
        importPath: "./dir10123456789012345678901234567890123456789"
      },
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });

  it("should produce multiline import if imports are too long", async expect => {
    // arrange
    const input: ImportModel[] = [
      {
        type: "Type101234567890123456789",
        alias: "Type101234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type201234567890123456789",
        alias: "Type201234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type301234567890123456789",
        alias: "Type301234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type401234567890123456789",
        alias: "Type401234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type501234567890123456789",
        alias: "Type501234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type601234567890123456789",
        alias: "Type601234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type701234567890123456789",
        alias: "Type701234567890123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      }
    ];
    const expected: SquashedImport[] = [
      {
        isMultiline: true,
        types: [
          "Type101234567890123456789, Type201234567890123456789, Type301234567890123456789, Type401234567890123456789, Type501234567890123456789,",
          "Type601234567890123456789, Type701234567890123456789"
        ],
        importPath: "./dir10123456789012345678901234567890123456789"
      }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });
});


