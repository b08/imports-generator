import { SquashedImport } from "../../src/squash/squashedImport.type";
import { areEquivalent } from "@b08/array";
import { IExpect } from "@b08/test-runner";

export function checkCompactImports(expect: IExpect, result: SquashedImport[], ex: SquashedImport[]): void {
  expect.equal(result.length, ex.length);
  ex.forEach(e => checkPair(expect, e, result));
}

function checkPair(expect: IExpect, ex: SquashedImport, result: SquashedImport[]): void {
  const pair = result.find(r => importsEqual(ex, r));
  expect.true(pair != null, "result should contain expected import");
}

function importsEqual(ex: SquashedImport, result: SquashedImport): boolean {
  return result.importPath === ex.importPath
    && result.isMultiline === ex.isMultiline
    && areEquivalent(result.types, ex.types);
}
