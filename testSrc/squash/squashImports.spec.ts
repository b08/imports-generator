import { describe } from "@b08/test-runner";
import { SquashedImport } from "../../src/squash/squashedImport.type";
import { squashImports } from "../../src/squash/squashImports";
import { ImportModel } from "../../src/types";
import { checkCompactImports } from "./checkCompactImports";

describe("squashImports", it => {
  it("should work with single import", async expect => {
    // arrange
    const input: ImportModel[] = [
      { type: "Type1", alias: "Type1", importPath: "." }
    ];
    const expected: SquashedImport[] = [
      { isMultiline: false, types: ["Type1"], importPath: "." }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });

  it("should work with aliased import", async expect => {
    // arrange
    const input: ImportModel[] = [
      { type: "Type1", alias: "Type2", importPath: "." }
    ];
    const expected: SquashedImport[] = [
      { isMultiline: false, types: ["Type1 as Type2"], importPath: "." }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });

  it("should not produce multiline if types + path is less than 120", async expect => {
    // arrange
    const input: ImportModel[] = [
      {
        type: "Type10123456789012345678901234567890123456789",
        alias: "Type20123456789012345678901234567890123456789",
        importPath: "./dir01234567890123456789"
      }
    ];
    const expected: SquashedImport[] = [
      {
        isMultiline: false,
        types: ["Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789"],
        importPath: "./dir01234567890123456789"
      }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });

  it("should produce multiline if types + path is longer than 120", async expect => {
    // arrange
    const input: ImportModel[] = [
      {
        type: "Type10123456789012345678901234567890123456789",
        alias: "Type20123456789012345678901234567890123456789",
        importPath: "./dir0123456789012345678901"
      }
    ];
    const expected: SquashedImport[] = [
      {
        isMultiline: true,
        types: ["Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789"],
        importPath: "./dir0123456789012345678901"
      }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(expect, result, expected);
  });
});


