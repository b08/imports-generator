import { test } from "@b08/test-runner";
import { imports } from "../src";
import { GeneratorOptions, ImportModel } from "../src/types";
import { generateImports } from "../src";

const options: GeneratorOptions = {
  linefeed: "\n",
  quotes: "\"",
  importTypesFromIndex: /.+State$/
};

test("razor should produce singleLine imports", expect => {
  // arrange
  const input: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" }
  ];
  const expected = `import { Type1 } from "./type1"`;

  // act
  const result = imports.generate(input, options);

  // assert
  expect.true(result.startsWith(expected));
});

test("razor should produce singleLine imports with index importing", expect => {
  // arrange
  const input: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" },
    { type: "Type1State", alias: "Type1State", importPath: "./type1.state" }
  ];
  const expected = `import { Type1 } from "./type1";
import { Type1State } from ".";
`;

  // act
  const result = imports.generate(input, options);

  // assert
  expect.equal(result, expected);
});


test("razor should produce multiLine imports", expect => {
  // arrange
  const input: ImportModel[] = [
    {
      type: "Type10123456789012345678901234567890123456789",
      alias: "Type20123456789012345678901234567890123456789",
      importPath: "./dir0123456789012345678901234567890123456789"
    }
  ];
  const expected = `import {
  Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789
} from "./dir0123456789012345678901234567890123456789";`;
  // act
  const result = imports.generate(input, options);

  // assert
  expect.true(result.startsWith(expected));
});

test("razor should produce multiLine imports", expect => {
  // arrange
  const input: ImportModel[] = [
    {
      type: "Type10123456789012345678901234567890123456789",
      alias: "Type20123456789012345678901234567890123456789",
      importPath: "./dir012345678901234567890"
    },
    {
      type: "Type301234567890123456789",
      alias: "Type401234567890123456789",
      importPath: "./dir012345678901234567890"
    }
  ];
  const expected = `import {
  Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789,
  Type301234567890123456789 as Type401234567890123456789
} from "./dir012345678901234567890";`;
  // act
  const result = imports.generate(input, options);

  // assert
  expect.true(result.startsWith(expected));
});

test("generator should produce singleLine imports", expect => {
  // arrange
  const input: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" }
  ];
  const expected = `import { Type1 } from "./type1"`;

  // act
  const result = generateImports(input, options);

  // assert
  expect.true(result.startsWith(expected));
});

test("generator produces singleLine imports with index importing", expect => {
  // arrange
  const input: ImportModel[] = [
    { type: "Type1", alias: "Type1", importPath: "./type1" },
    { type: "Type1State", alias: "Type1State", importPath: "./type1.state" }
  ];
  const expected = `import { Type1 } from "./type1";
import { Type1State } from ".";
`;

  // act
  const result =  generateImports(input, options);

  // assert
  expect.equal(result, expected);
});

test("generator should produce multiLine imports", expect => {
  // arrange
  const input: ImportModel[] = [
    {
      type: "Type10123456789012345678901234567890123456789",
      alias: "Type20123456789012345678901234567890123456789",
      importPath: "./dir0123456789012345678901234567890123456789"
    }
  ];
  const expected = `import {
  Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789
} from "./dir0123456789012345678901234567890123456789";`;
  // act
  const result = generateImports(input, options);

  // assert
  expect.true(result.startsWith(expected));
});

test("generator should produce multiLine imports", expect => {
  // arrange
  const input: ImportModel[] = [
    {
      type: "Type10123456789012345678901234567890123456789",
      alias: "Type201234567890123456789",
      importPath: "./dir012345678901234567890"
    },
    {
      type: "Type30123456789012345678901234567890123456789",
      alias: "Type401234567890123456789",
      importPath: "./dir012345678901234567890"
    }
  ];
  const expected = `import {
  Type10123456789012345678901234567890123456789 as Type201234567890123456789,
  Type30123456789012345678901234567890123456789 as Type401234567890123456789
} from "./dir012345678901234567890";`;
  // act
  const result = generateImports(input, options);

  // assert
  expect.true(result.startsWith(expected));
});
