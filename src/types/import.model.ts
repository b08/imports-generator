export interface ImportModel {
    type: string;
    alias: string;
    importPath: string;
}
