import { generator, lines } from "@b08/generator";
import { SquashedImport, squashImports } from "../squash";
import { tryImportFromIndex } from "../tryImportFromIndex";
import { GeneratorOptions, ImportModel } from "../types";

export const generateImports = generator(function generateImports(this: GeneratorOptions, imports: ImportModel[]): string {
  const derived = tryImportFromIndex(imports, this);
  const compact = squashImports(derived);
  return compact.map(i => i.isMultiline ? multiline(i) : singleLine(i)).map(l => l + "\n").join("");
});

function multiline(imp: SquashedImport): string {
  return `import {
${lines(imp.types, l => `  ${l}`)}
} from '${imp.importPath}';`;
}

function singleLine(imp: SquashedImport): string {
  return `import { ${imp.types[0]} } from '${imp.importPath}';`;
}
