import { last } from "@b08/array";
import { GeneratorOptions, ImportModel } from "./types";

export function tryImportFromIndex(imports: ImportModel[], options: GeneratorOptions): ImportModel[] {
  if (options.importTypesFromIndex == null) {
    return imports;
  }
  return imports.map(i => tryFromIndex(i, options));
}

function doesTypeMatch(imp: ImportModel, options: GeneratorOptions): boolean {
  if (Array.isArray(options.importTypesFromIndex)) {
    return options.importTypesFromIndex.some(i => imp.type.match(i) != null);
  }

  return imp.type.match(options.importTypesFromIndex) != null;
}

function tryFromIndex(imp: ImportModel, options: GeneratorOptions): ImportModel {
  if (!doesTypeMatch(imp, options)) { return imp; }
  return {
    ...imp,
    importPath: stripLastSegment(imp.importPath)
  };
}

function stripLastSegment(path: string): string {
  const split = path.split("/");
  if (path.length < 2 || last(split) === "..") { return path; }
  return split.slice(0, split.length - 1).join("/");
}
