// warning! This code was generated by @b08/razor
// manual changes to this file will be overwritten next time the code is regenerated.
import { Generator, GeneratorOptions } from "@b08/generator";
import { SquashedImport } from "../squash";

function generateContent(imp: SquashedImport, gen: Generator): Generator {
  gen = gen.append(`import {`);
  gen = gen.eol();
  for (let type of imp.types) {
    gen = gen.append(`  `);
    gen = gen.append((type).toString());
    gen = gen.eol();
  }
  gen = gen.append(`} from `);
  gen = gen.apostrophe();
  gen = gen.append((imp.importPath).toString());
  gen = gen.apostrophe();
  gen = gen.append(`;`);
  gen = gen.eol();
  return gen;
}

function generate(imp: SquashedImport, options: GeneratorOptions): string {
  return generateContent(imp, new Generator(options)).toString();
}

export const multiLine = {
  generate,
  generateContent
};
