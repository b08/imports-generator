export interface SquashedImport {
  isMultiline: boolean;
  importPath: string;
  types: string[];
}
