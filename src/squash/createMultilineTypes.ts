const maxMultilineLength = 138;

export function createMultilineTypes(joinedTypes: string[]): string[] {
  const split = splitIntoSegments(joinedTypes);
  return split.map((s, i) => createLine(s, i === split.length - 1));
}

function createLine(types: string[], isLast: boolean): string {
  return isLast
    ? types.join(", ")
    : types.map(t => t + ",").join(" ");
}

function splitIntoSegments(joinedTypes: string[]): string[][] {
  const result: string[][] = [];
  let index = 0;
  while (index < joinedTypes.length) {
    const next = findNextSegmentIndex(joinedTypes, index);
    result.push(joinedTypes.slice(index, next));
    index = next;
  }

  return result;
}

function findNextSegmentIndex(joinedTypes: string[], start: number): number {
  let end = start + 1;
  let length = joinedTypes[start].length;
  while (end < joinedTypes.length) {
    length += joinedTypes[end].length + 2;
    if (length > maxMultilineLength) { return end; }
    end++;
  }

  return end;
}
