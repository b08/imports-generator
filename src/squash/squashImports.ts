import { groupBy, unique } from "@b08/flat-key";
import { ImportModel } from "../types";
import { SquashedImport } from "./squashedImport.type";
import { createMultilineTypes } from "./createMultilineTypes";

const maxSingleLength = 120;

export function squashImports(imports: ImportModel[]): SquashedImport[] {
  const uniqueImports = unique(imports);
  const grouped = groupBy(uniqueImports, imp => imp.importPath);
  return grouped.values().map(getGroupImports);
}

function getGroupImports(imports: ImportModel[]): SquashedImport {
  const joinedTypes = imports.map(joinType);
  const length = joinedTypes.reduce((a, b) => a + b.length, 0) + (joinedTypes.length - 1) * 2;
  const importPath = imports[0].importPath;
  const isMultiline = importPath.length + length > maxSingleLength;
  const types = isMultiline ? createMultilineTypes(joinedTypes) : [joinedTypes.join(", ")];
  return { isMultiline, types, importPath };
}

function joinType(imp: ImportModel): string {
  return imp.type === imp.alias
    ? imp.alias
    : `${imp.type} as ${imp.alias}`;
}
