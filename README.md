# @b08/imports-generator, seeded from @b08/generator-seed, library type: generator
Partial generator, dedicated to generate "imports" section of the file.
Main generator is to create ImportModel[] and feed it here instead of rendering imports on its own.

# features
1. Grouping several imports into one import line, result will look like this
```
import { Type1, Types } from "./file";
```
2. Multiline imports. When import line length overflows limit, imports will be split into several lines like this
```
import {
  Type1, Type2, Type3, Type4
} from "./file";
```
// todo - splitting types into several lines is not currently supported.